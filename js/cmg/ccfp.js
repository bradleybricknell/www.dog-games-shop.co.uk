// JavaScript Document

var http_request = false;
var formid = false;

function makeRequest(url, parameters) {
	http_request = false;
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			// set type accordingly to anticipated content type
			//http_request.overrideMimeType('text/xml');
			http_request.overrideMimeType('text/html');
		}
	} else if (window.ActiveXObject) { // IE
	try {
		http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
	if (!http_request) {
		alert('Cannot create XMLHTTP instance');
		return false;
	}
	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url + parameters, true);
	http_request.send(null);
}

function reloadImage() {
	var myDate = new Date();
	mageUrl = baseUrl();
	document.getElementById('captchaImage').src = mageUrl + 'ccfp/captcha?t=' + myDate.getTime();
}

function checkCaptcha(obj) {
	//var getstr = "?security_code=" + obj;
	//mageUrl = baseUrl();
	//makeRequest(mageUrl + 'ccfp/verify', getstr);
}

function baseUrl() {
	return document.getElementById('baseurl').innerHTML;
}

function alertContents() {
    console.log("Alert contents");
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			result = http_request.responseText;
			if (result == "false") {
				reloadImage();
				alert('You entered the security code incorrectly! Please try again.');
				document.getElementById('security_code').value = "";
				document.getElementById('security_code').focus();
				return false;
			}
			else if (result == "true") {
				formid.submit();
				return true;
			}
		} else {
			alert('There was a problem with the request.');
			return false;
		}
	}
}

//have a form validated by class="required" by adding onsubmit="return checkRequired(this)" to the form tag
function checkRequired(form){
    console.log(document.getElementById("privacy_policy").checked);
	var els=form.elements;
	errorFields=new Array("Please provide valid information for: \n");
	var failed=0;
	for(b=0;b<els.length;b++){
		if(els[b].className=="required" || els[b].className.indexOf('required') >= 0){
			if(els[b].value.length<2){
				if(els[b].title){
					errorFields.push(els[b].title)
				}else{
					errorFields.push(els[b].name)
				}
				failed=1
			}
			if(els[b].name.indexOf('email') >= 0 && failed==0){
				if(!is_valid_email(els[b].value)){
					if(els[b].title){
						errorFields.push(els[b].title)
					}else{
						errorFields.push(els[b].name)
					}
				}
			}
		}
		failed=0
	}
    console.log("Failed = " + failed);
	
	//display errors
	if(errorFields.length>1){
        console.log("All is not well...");
		var alertText="";
		for(a=0;a<errorFields.length;a++){
			alertText+=errorFields[a]+"\n"
		}
		alert(alertText);
		return false;
	}else{
        if (document.getElementById("privacy_policy").checked){
		  return true;
        } else {
          alert("Please agree to our Terms and Conditions.");
        }
        
	}
}

function is_valid_email(email){
	return/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email)
}

function checkAll(form) {
	formid = form;
	if (checkRequired(form)) {
		//if(checkCaptcha(document.getElementById('security_code').value)) {
			//return false; // Set to return false on purpose. Creates potential security hole otherwise.
		//}
		//else {
			//return false;
		//}
        console.log("Check all ok");
        return true;
	}
	else {
        console.log("Check all not ok");
		return false;
	}
}