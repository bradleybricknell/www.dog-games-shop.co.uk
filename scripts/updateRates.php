<?php
// LEXEL custom script
    require_once dirname(dirname(__FILE__)) . '/app/Mage.php';
    Mage::app();
    try {
        $service = Mage::getStoreConfig('currency/import/service');
        try {
            $importModel = Mage::getModel(Mage::getConfig()->getNode('global/currency/import/services/' . $service . '/model')->asArray());
        } catch (Exception $e) {
            $importWarnings[] = Mage::helper('directory')->__('FATAL ERROR:') . ' ' . Mage::throwException(Mage::helper('directory')->__('Unable to initialize the import model.'));
        }
        $rates = $importModel->fetchRates();
        if ($rates != null) {
            Mage::getModel('directory/currency')->saveRates($rates);
        }
    } catch (Exception $e) {
        return false;
        echo 'there is an error - better luck next time :)';
    }
?>
