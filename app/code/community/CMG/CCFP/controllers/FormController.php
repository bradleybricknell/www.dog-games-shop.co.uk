<?php
 
class CMG_CCFP_FormController extends Mage_Core_Controller_Front_Action
{	
	public function indexAction()
	{
		$verify = Mage::getSingleton('core/session')->getFormSubmitted();
		
		//if ($verify == 'Yes') {
			Mage::getSingleton('core/session')->setFormSubmitted();
			$params = $this->getRequest()->getParams();
			
			if (isset($params['configName'])) {
				include(Mage::getRoot().'/code/community/CMG/CCFP/form-configurations/'.$params['configName'].'.php');
				
				$message = $config['message'];
				
				foreach($fields as $value) {
					if (isset($params[$value])) {
						$message .= "\n$labels[$value] $params[$value]";
					}
				}
				
				$mail = new Zend_Mail();
				$mail->setBodyText($message);
				$mail->setFrom($params[$config['emailID']], $params[$config['nameID']]);
				$mail->addTo($config['sendto']);
				$mail->setSubject($config['subject']);
				try {
					$mail->send();
				}        
				catch(Exception $ex) {
					Mage::getSingleton('core/session')->addError('There was an error submitting your request.');
		 
				}
		 
				//Redirect back to index action of this controller
				$this->_redirect($config['redirect']);
			}
			else {
				die('There was an error with the form!');
			}
		//}
		//else {
		//	die('An error occured! Please go back and try submitting the form again.');
		//}
	}
}