<?php

$fields = array(
/* List the name of each field element below. Put each between double quotes(" ") and end every line with a comma(,). The name should be the exact same spelling and capitalization as the name attribute in the <input> or <textarea> tag it is referenceing. Whatever order these are in is the order they will appear in the email that is sent. */

"name",
"company",
"address",
"city",
"state",
"zip",
"phone",
"email",
"info1",
"info2",
"info3",

);


$labels = array (
/* Everything before the "=>" should be the same as above. After the "=>" is what will appear in the email before the matching content from the form. (e.g. if you put: "email" => "Email Address:", then the email will have: "Email Address: someguy@domain.com" for the email address. */

"name" => "Name:",
"company" => "Company:",
"address" => "Address:",
"city" => "City:",
"state" => "State:",
"zip" => "Zip:",
"phone" => "Phone:",
"email" => "Email:",
"info1" => "Info about Product 1:",
"info2" => "Info about Product 2:",
"info3" => "Info about Product 3:",

);

$config = array(
/* Used to change configuration settings such as where to send the form submissions, the subject and where to send the user after they submit the form */

"nameID" => "name", // Insert the id of the field containing the users name, this will be used in sending you the email.

"emailID" => "email", // Insert the id of the field containing the users email address, this will use the users email address as the "from" email address.

"sendto" => "user@domain.com", // Insert the email addresses you want the form to send the submissions to, you may seperate several email addresses using commas(,)

"subject" => "Brochure Request Submission", // Insert the subject you want the email to have here

"message" => "*** Brochure Request Submission ***", // Insert anything you want in the body of the email (before the contents of the form) here! You can start a new line by using "\n" (without quotes). Use a backslash(\) before any double quotes(" ") or dollar signs($) to prevent errors.

"redirect" => "/", // The web address of the thank you page or other page the user is redirected to after submitting the form, note that it is relative to the page the form is on

);
