<?php

class Lexel_Harnesscalc_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        $this->loadLayout()->renderLayout();
    }
	
	protected function autocompleteAction() {
		
		$term = $_REQUEST['term'];
		$breeds = Mage::getModel('harnesscalc/breeds')->getBreeds($term);	
		return $breeds;	
	}
	
	/**
	** Harness
	**/
	public function submitAction()
	{
		$girth = $this->getRequest()->getParam('girth_size');
		$measurement = $this->getRequest()->getParam('measurement');
		$breedId = $this->getRequest()->getParam('breed_id');
		$gender = $this->getRequest()->getParam('gender');
		$isPuppy = $this->getRequest()->getParam('is_puppy');
		
		if ($measurement === 'inches') {
			$girth = $this->convertToCm($girth);	
		}
		
		$dogSize = Mage::getModel('harnesscalc/breeds')->load($breedId);
		
		$harnessSize = $this->getHarnessSize($dogSize, $isPuppy, $gender);
		$this->filterCollection($girth, $harnessSize);
		
	}
	
	public function convertToCm ($girth) {
		$girth = (float)$girth * 2.54;
		return number_format($girth, 2);
	}
	
	public function getHarnessSize($dogSize, $isPuppy, $gender) {
		if ($isPuppy == 1) {
			if ($gender == 'male') {
			    $harnessSize = $dogSize->getMaleHarnessPuppySize();
			} else {
				$harnessSize = $dogSize->getFemaleHarnessPuppySize();
			}
		} else {
			if ($gender == 'male') {
			    $harnessSize = $dogSize->getMaleHarnessSize();	
			} else {
				$harnessSize = $dogSize->getFemaleHarnessSize();
			}
		}
		
		return $harnessSize;
	}
	
	public function filterCollection ($girth, $harnessSize) {
		
		// Get all products with the "Harness Size" attribute. May require more depending on feedback, yo.
		$collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('sizeharness', array('notnull' => true))
				->addAttributeToFilter('status', array('eq' => 1))
				->addAttributeToFilter('visibility', array('neq' => 1));				          
									        		
		$this->calculate($collection, $girth, $harnessSize);				
	}
	
	public function calculate ($collection, $girth, $harnessSize) {
		echo '<p class="calc_sub">Based on the information submitted these are our recommended harnesses for your dog:</p>';
		echo '<ul>';
		$resultsCount = 0;			
		foreach ($collection as $product) {
			$product = Mage::getModel('catalog/product')->load($product->getId());
			$attributeOption = $product->getAttributeText('sizeharness');
			
			// Remove all but digits
			$attributeOption = preg_replace('/\D+/', ',', $attributeOption);
			
			// 'Size' is first character... store it!
			$harnessSizeDigit = substr($attributeOption, 0, 1);			
			
			// Now get rid of it.
			$attributeOption = substr($attributeOption, 1);
			
			// Remove commas
			$attributeOption = str_replace(',','', $attributeOption);
			
			// Remove last 4 chars - inches don't go over 2 digits each
			$attributeOption = substr($attributeOption, 0, -4);
			
			// Add comma
			$attributeOption = chunk_split($attributeOption, 2, ',');
 
			// Remove
			$attributeOption = substr($attributeOption, 0, -1);
			
			// If check here for 3 digit upper lims
			if (strlen($attributeOption) > 6) {
				
				// Better - remove 2nd to last character
				$lastChar = substr($attributeOption, -1);
				$attributeOption = substr($attributeOption, 0, -1);
				$attributeOption = substr($attributeOption, 0, -1);
				$attributeOption .= $lastChar;			
			}
			
			// Split
			list($lowerLim, $upperLim) = explode(",", $attributeOption);
			
			// Don't enter if there aren't any matches for dog/puppy size stored.
			if ($harnessSize === $harnessSizeDigit) {
				if (strpos($attributeOption, (int)$girth) !== false || ((int)$lowerLim <= $girth && (int)$girth <= $upperLim)) {
					
					$addToCart = "'http://www.cosy-dogs.com/checkout/cart/add/uenc/aHR0cDovL3d3dy5hYy5sZXhlbC5jby51ay9tZW4vby1uZWlsbC1ibHVlLW1lbGFuZ2UtbG9nby10LXNoaXJ0LW1lbnMuaHRtbA,,/product/" .  $product->getId() . "/form_key/" . Mage::getSingleton('core/session')->getFormKey() . "'";
					
					echo '<li class="item">';
					echo     '<p class="product-image" id="p_' . $product->getSku() . '>';
					echo         '<a href="' . $product->getProductUrl() . '" title="' . $product->getName() . '">';
					echo             '<img src="' . Mage::helper('catalog/image')->init($product, 'small_image')->resize(135) . '" alt="' . $product->getName() . '" />';
					echo         '</a>';
					echo     '</p>';
					echo     '<h4><a href="' . $product->getProductUrl() . '" title="' . $product->getName() . '">' . $product->getName() . '</a></h4>';
					echo	 '<p>Girth size: ' . $product->getAttributeText("sizeharness") . '</p>';
					echo     '<div class="price-box">';
					echo         '<span class="price">' . Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol() . number_format($product->getFinalPrice(), 2, '.', '') . '</span>';
					echo     '</div>';
					echo     '<div class="actions">';
							 if ($product->getInStock() !== 0) :
					echo          '<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(' . $addToCart . ')"><span><span>Add to Cart</span></span></button>';
							 else :
					echo         '<p class="availability out-of-stock"><span>Out of stock</span></p>';
							 endif;  
								 
					echo '</li>';
					$resultsCount++;
				};
			};
			
		}
		echo '</ul>';
		
		if ($resultsCount === 0) {
			echo "<p class='calc_sub'>Sorry, but there were no matches for your Dog's girth size.</p>";	
		}	
	}
	
}