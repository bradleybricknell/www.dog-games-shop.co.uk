<?php

class Lexel_Harnesscalc_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        $this->loadLayout()->renderLayout();
    }
	
	protected function autocompleteAction() {
		
		$term = $_REQUEST['term'];
		$breeds = Mage::getModel('harnesscalc/breeds')->getBreeds($term);	
		return $breeds;	
	}
	
	/**
	** Harness
	**/
	public function getProductModel() {
		$productModel = Mage::getModel('catalog/product');
		return $productModel;	
	}
	
	public function submitAction()
	{
		$girth = $this->getRequest()->getParam('girth_size');
		$measurement = $this->getRequest()->getParam('measurement');
		$breedId = $this->getRequest()->getParam('breed_id');
		$gender = $this->getRequest()->getParam('gender');
		$isPuppy = $this->getRequest()->getParam('is_puppy');
		
		$productModel = Mage::getModel('catalog/product');
		
		if ($measurement === 'inches') {
			$girth = $this->convertToCm($girth);	
		}
		
		$dogSize = Mage::getModel('harnesscalc/breeds')->load($breedId);
		
		$harnessSize = $this->getHarnessSize($dogSize, $isPuppy, $gender);
		$this->filterCollection($girth, $harnessSize);
		
	}
	
	public function convertToCm ($girth) {
		$girth = (float)$girth * 2.54;
		return number_format($girth, 2);
	}
	
	public function getHarnessSize($dogSize, $isPuppy, $gender) {
		if ($isPuppy == 1) {
			if ($gender == 'male') {
			    $harnessSize = $dogSize->getMaleHarnessPuppySize();
			} else {
				$harnessSize = $dogSize->getFemaleHarnessPuppySize();
			}
		} else {
			if ($gender == 'male') {
			    $harnessSize = $dogSize->getMaleHarnessSize();	
			} else {
				$harnessSize = $dogSize->getFemaleHarnessSize();
			}
		}
		
		return $harnessSize;
	}
	
	public function filterCollection ($girth, $harnessSize) {
		
		// Get all products with the "Harness Size" attribute.
		$collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('attribute_set_id','17') // Perfect fit harness attribute set
				->addAttributeToFilter('type_id', 'bundle')
				->addAttributeToFilter('status', array('eq' => 1))
				->addAttributeToFilter('visibility', array('neq' => 1));				          
									        		
		$this->calculate($collection, $girth, $harnessSize);				
	}
	
	public function calculate ($collection, $girth, $harnessSize) {
		echo '<p class="calc_sub">Based on the information submitted these are our recommended harnesses for your dog:</p>';
		echo '<ul>';
		$resultsCount = 0;
        
				
		foreach ($collection as $product) {
			$product = $this->getProductModel()->load($product->getId());
			
			// Name
			$productName = $product->getName();
			
			// Get Harness ranges tiny/15mm/20mm/40mm
			$productNameDigits = substr($productName, 0, 2);
			$productNameDigits = (int) $productNameDigits;
			
			// If not an integer, it's the tiny range
			if (is_numeric($productNameDigits) !== 0) {
			    $range = $productNameDigits;
			};
			 
			if ($range == 0) {
				$range = "TY"; 
			};
			
                     
            // To do - remove above and use
			preg_match('#\((.*?)\)#', $productName, $match);
            
            $productName = $match[1];
            
            // Get Girth - remove all but digits
			$productName = preg_replace('/\D+/', ',', $productName);
            
				// Add comma
				$productName = substr($productName, 0, -1);
				
				// Split
				list($lowerLim, $upperLim) = explode(",", $productName);
				
				// Don't enter if there aren't any matches for dog/puppy size stored.
                //strpos($productName, (int)$girth) !== false || 
					if (((int)$lowerLim <= $girth && (int)$girth <= $upperLim && ($range == $harnessSize || strpos($harnessSize, $range) !== false))) {
						
						$addToCart = "'http://www.dog-games-shop.co.uk/checkout/cart/add/uenc/aHR0cDovL3d3dy5hYy5sZXhlbC5jby51ay9tZW4vby1uZWlsbC1ibHVlLW1lbGFuZ2UtbG9nby10LXNoaXJ0LW1lbnMuaHRtbA,,/product/" .  $product->getId() . "/form_key/" . Mage::getSingleton('core/session')->getFormKey() . "'";
						
						echo '<li class="item"' . $lowerLim . ' ' . $upperLim . '>';
						echo     '<p class="product-image" id="p_' . $product->getSku() . '>';
						echo         '<a href="' . $product->getProductUrl() . '" title="' . $product->getName() . '">';
						echo             '<img src="' . Mage::helper('catalog/image')->init($product, 'small_image')->resize(135) . '" alt="' . $product->getName() . '" />';
						echo         '</a>';
						echo     '</p>';
						echo     '<h4><a href="' . $product->getProductUrl() . '" title="' . $product->getName() . '">' . $product->getName() . '</a></h4>';
						echo     '<div class="actions">';
								 if ($product->getInStock() !== 0) :
						echo          '<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(' . $addToCart . ')"><span><span>Add to Cart</span></span></button>';
								 else :
						echo         '<p class="availability out-of-stock"><span>Out of stock</span></p>';
								 endif;  
									 
						echo '</li>';
						$resultsCount++;
					};
			
		}
		echo '</ul>';
		if ($resultsCount === 0) {
			echo "<p class='calc_sub'>Sorry, but there were no matches for your Dog's girth size.</p>";	
		}		
						
	}
	
}