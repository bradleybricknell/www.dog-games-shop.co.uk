<?php
 
class Lexel_Harnesscalc_Adminhtml_BreedsController extends Mage_Adminhtml_Controller_Action
{  
     protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('breeds/items')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Breeds Manager'), Mage::helper('adminhtml')->__('Breeds Manager'));
        return $this;
    }   
   
    public function indexAction() {
        $this->_initAction();
		    
        $this->_addContent($this->getLayout()->createBlock('harnesscalc/adminhtml_breeds'));
        $this->renderLayout();
		
    }
	
	public function editAction()
    {
        $breedId     = $this->getRequest()->getParam('id');
        $breedsModel  = Mage::getModel('harnesscalc/breeds')->load($breedId);
		
		
 
        if ($breedsModel->getId() || $breedId == 0) {
			
 
            Mage::register('breeds_data', $breedsModel);
 
            $this->loadLayout();
            $this->_setActiveMenu('breeds/items');
			
           
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Breeds Manager'), Mage::helper('adminhtml')->__('Breeds Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));
			
           
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			
           
            $this->_addContent($this->getLayout()->createBlock('harnesscalc/adminhtml_breeds_edit'))
                 ->_addLeft($this->getLayout()->createBlock('harnesscalc/adminhtml_breeds_edit_tabs')); // Here
				 
               
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('breeds')->__('Breed does not exist'));
            $this->_redirect('*/*/');
        }
    }
   
    public function newAction()
    {
        $this->_forward('edit');
    }
   
    public function saveAction()
    {
        if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
                $breedsModel = Mage::getModel('harnesscalc/breeds');
               
                $breedsModel->setId($this->getRequest()->getParam('id'))
                    ->setBreedName($postData['breed_name'])
					->setMaleHarnessSize($postData['male_harness_size'])
					->setFemaleHarnessSize($postData['female_harness_size'])
					->setMaleHarnessPuppySize($postData['male_harness_puppy_size'])
					->setFemaleHarnessPuppySize($postData['female_harness_puppy_size'])
                    ->save();
               
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Breed was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setbreedsData(false);
 
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setbreedsData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
   
    public function deleteAction()
    {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $breedsModel = Mage::getModel('harnesscalc/breeds');
               
                $breedsModel->setId($this->getRequest()->getParam('id'))
                    ->delete();
                   
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Breed was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
	
	public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('harnesscalc/adminhtml_breeds_grid')->toHtml()
        );
    }
       
 
  
}