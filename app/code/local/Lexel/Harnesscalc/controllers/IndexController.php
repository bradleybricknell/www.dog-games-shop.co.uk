<?php

class Lexel_Harnesscalc_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        $this->loadLayout()->renderLayout();
    }

    protected function autocompleteAction() {
        $term = $_REQUEST['term'];
        $breeds = $this->getBreedModel()->getBreeds($term);
        return $breeds;
    }

    public function getBreedModel() {
        return Mage::getModel('harnesscalc/breeds');
    }

    public function getImageHelper() {
        return Mage::helper('catalog/image');
    }

    public function getProductModel() {
        return Mage::getModel('catalog/product');
    }

    public function submitAction()
    {
        $girth = $this->getRequest()->getParam('girth_size');
        $measurement = $this->getRequest()->getParam('measurement');
        $breedId = $this->getRequest()->getParam('breed_id');
        $gender = $this->getRequest()->getParam('gender');
        $isPuppy = $this->getRequest()->getParam('is_puppy');

        $productModel = Mage::getModel('catalog/product');

        if ($measurement === 'inches') {
            $girth = $this->convertToCm($girth);
        }

        $dogSize = $this->getBreedModel()->load($breedId);
        $harnessSize = $this->getHarnessSize($dogSize, $isPuppy, $gender);
        $rangeSize = $this->getRangeSize($dogSize);
        $this->filterCollection($girth, $harnessSize, $rangeSize);

    }

    public function convertToCm ($girth) {
        $girth = (float)$girth * 2.54;
        return number_format($girth, 2);
    }

    public function strposa($haystack, $needle, $offset=0) {
        if(!is_array($needle)) $needle = array($needle);
        foreach($needle as $query) {
            if(strpos($haystack, $query, $offset) !== false) return true;
        }
        return false;
    }

    public function getRangeSize($breed) {
        $rangeSize = $breed->getRangeSize();
        // $rangeSize = substr($rangeSize, 3);
        if (!$rangeSize) {
            $rangeSize = "";
        }
        if (strpos($rangeSize, ", ") !== false) {
            $rangeSize = str_replace(", ", ",", $rangeSize);
        }
        if (strpos($rangeSize, ",") !== false) {
            $rangeSize = explode(",", $rangeSize);
            $rangeSize = array_map('strtolower', $rangeSize);
        } else {
            $rangeSize = strtolower($rangeSize);
        }
        return $rangeSize;
    }

    public function getHarnessSize($dogSize, $isPuppy, $gender) {
        if ($isPuppy == 1) {
            if ($gender == 'male') {
                $harnessSize = $dogSize->getMaleHarnessPuppySize();
            } else {
                $harnessSize = $dogSize->getFemaleHarnessPuppySize();
            }
        } else {
            if ($gender == 'male') {
                $harnessSize = $dogSize->getMaleHarnessSize();
            } else {
                $harnessSize = $dogSize->getFemaleHarnessSize();
            }
        }

        return $harnessSize;
    }

    public function filterCollection ($girth, $harnessSize, $rangeSize) {
        $collection = $this->getProductModel()->getCollection()
            ->addAttributeToFilter('attribute_set_id','17')
            ->addAttributeToFilter('type_id', 'bundle')
            ->addAttributeToFilter('status', array('eq' => 1))
            ->addAttributeToFilter('visibility', array('neq' => 1));
        if (!is_array($rangeSize)) {
            $collection->addAttributeToFilter('sku', array('like'=> '%' . $rangeSize . '%'));
        } else {
            $collection->addAttributeToFilter(array(
                array('attribute'=>'sku','like'=> '%' . (string)$rangeSize[0] . '%'),
                array('attribute'=>'sku','like'=> '%' . (string)$rangeSize[1] . '%'),
                array('attribute'=>'sku','like'=> '%' . (string)$rangeSize[2] . '%'),
                array('attribute'=>'sku','like'=> '%' . (string)$rangeSize[3] . '%')
            ));
        }
        $this->calculate($collection, $girth, $harnessSize, $rangeSize);
    }

    public function calculate ($collection, $girth, $harnessSize, $rangeSize) {
        if ($this->getRequest()->getParam('is_puppy') == 1) {
            $output = $this->getContactHtml();
        } else {
            $output =  '<p class="calc_sub">Based on the information submitted these are our recommended harnesses for your dog:</p>';
            $output .= '<ul>';
            $resultsCount = 0;

            $harnessSizes = explode(",", $harnessSize);
            foreach ($collection as $product) {
                $product = $this->getProductModel()->load($product->getId());
                $productName = $product->getName();

                // Get Harness ranges tiny/15mm/20mm/40mm
                $productNameDigits = substr($productName, 0, 2);
                $productNameDigits = (int) $productNameDigits;
                preg_match('#\((.*?)\)#', $productName, $match);

                $productName = $match[1];

                // Get Girth - remove all but digits
                $productName = preg_replace('/\D+/', ',', $productName);

                // Add comma
                $productName = substr($productName, 0, -1);

                $productSku = $product->getSku();
                if (is_array($rangeSize)) {
                    $result = $this->strposa($rangeSize, $productSku);
                    $rangeIsArray = true;
                    $calculatorFilters = '"( . $rangeIsArray . " && " . $result . ")"';
                } else {
                    $rangeIsArray = false;
                    $calculatorFilters = '"stripos($productSku, $rangeSize) !== false"';
                }

                list($lowerLim, $upperLim) = explode(",", $productName);
                if ((int)$lowerLim <= $girth && (int)$girth <= $upperLim && $calculatorFilters) {
                    $addToCart = "'http://www.dog-games-shop.co.uk/checkout/cart/add/uenc/";
                    $addToCart .= "aHR0cDovL3d3dy5hYy5sZXhlbC5jby51ay9tZW4vby1uZWlsbC1ibHVlLW1lbGFuZ2UtbG9nby10LXNoaXJ0LW1lbnMuaHRtbA,,";
                    $addToCart .= "/product/" .  $product->getId() . "/form_key/" . Mage::getSingleton('core/session')->getFormKey() . "'";

                    $output .=  '<li class="item">';
                    $output .=      '<p class="product-image" id="p_' . $product->getSku() . '">';
                    $output .=          '<a href="' . $product->getProductUrl() . '" title="' . $product->getName() . '">';
                    $output .=              '<img src="' . $this->getImageHelper()->init($product, 'small_image')->resize(135) . '" alt="' . $product->getName() . '" />';
                    $output .=          '</a>';
                    $output .=      '</p>';
                    $output .=      '<h4><a href="' . $product->getProductUrl() . '" title="' . $product->getName() . '">' . $product->getName() . '</a></h4>';
                    $output .=      '<div class="actions">';

                    if ($product->getInStock() !== 0) {
                        $output .=      '<button type="button" title="Add to Cart" class="button btn-cart" onclick="setLocation(' . $addToCart . ')"><span><span>Add to Cart</span></span></button>';
                    } else {
                        $output .=      '<p class="availability out-of-stock"><span>Out of stock</span></p>';
                    }

                    $output .=      '</div>';
                    $output .=  '</li>';
                    $resultsCount++;
                };

            }
            $output .= '</ul>';
        }
        if ($resultsCount === 0) {
            $output = $this->getContactHtml();
        }
        echo $output;
    }

    public function getContactHtml() {
        $output =  '<p class="calc_sub">Please contact our sales team for a recommendation on 01684569553 ';
        $output .= 'or email us at <a href="mailto:info@dog-games.co.uk">info@dog-games.co.uk</a></p>';
        return $output;
    }
}
