<?php
 
class Lexel_Harnesscalc_Block_Adminhtml_Breeds extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_breeds';
        $this->_blockGroup = 'harnesscalc';
        $this->_headerText = Mage::helper('harnesscalc')->__('Breeds Manager');
        $this->_addButtonLabel = Mage::helper('harnesscalc')->__('Add Breed');
        parent::__construct();
    }
	
	
}

?>