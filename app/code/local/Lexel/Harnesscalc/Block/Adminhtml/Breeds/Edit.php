<?php
class Lexel_Harnesscalc_Block_Adminhtml_Breeds_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
               
        $this->_objectId = 'id';
        $this->_blockGroup = 'harnesscalc';
        $this->_controller = 'adminhtml_breeds';
 
        $this->_updateButton('save', 'label', Mage::helper('harnesscalc')->__('Save Breed'));
        $this->_updateButton('delete', 'label', Mage::helper('harnesscalc')->__('Delete Breed'));
    }
 
    public function getHeaderText()
    {
        if( Mage::registry('breeds_data') && Mage::registry('breeds_data')->getId() ) {
            return Mage::helper('harnesscalc')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('breeds_data')->getbreedName()));
        } else {
            return Mage::helper('harnesscalc')->__('Add Breed');
        }
    }
}
?>
