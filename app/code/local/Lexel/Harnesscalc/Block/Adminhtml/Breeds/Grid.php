<?php
 
class Lexel_Harnesscalc_Block_Adminhtml_Breeds_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('breedsGrid');
        // This is the primary key of the database
        $this->setDefaultSort('breed_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('harnesscalc/breeds')->getCollection(); // This is the problem. Models yo.
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('breed_id', array(
            'header'    =>  Mage::helper('harnesscalc')->__('ID'),
            'align'     => 'left',
            'width'     => '100px',
            'index'     => 'breed_id',
        ));
 
        $this->addColumn('breed_name', array(
            'header'    =>  Mage::helper('harnesscalc')->__('Breed Name'),
            'align'     => 'left',
            'index'     => 'breed_name',
        ));
		
		$this->addColumn('male_harness_size', array(
            'header'    =>  Mage::helper('harnesscalc')->__('Male Harness Size'),
            'align'     => 'left',
            'index'     => 'male_harness_size',
        ));
		
		$this->addColumn('female_harness_size', array(
            'header'    =>  Mage::helper('harnesscalc')->__('Female Harness Size'),
            'align'     => 'left',
            'index'     => 'female_harness_size',
        ));
		
		$this->addColumn('male_harness_puppy_size', array(
            'header'    =>  Mage::helper('harnesscalc')->__('Male Puppy Harness Size'),
            'align'     => 'left',
            'index'     => 'male_harness_puppy_size',
        ));
		
		$this->addColumn('female_harness_puppy_size', array(
            'header'    =>  Mage::helper('harnesscalc')->__('Female Puppy Harness Size'),
            'align'     => 'left',
            'index'     => 'female_harness_puppy_size',
        ));

		$this->addColumn('date_added', array(
            'header'    =>  Mage::helper('harnesscalc')->__('Date Added'),
            'align'     => 'left',
            'index'     => 'date_added',
        ));
		

        return parent::_prepareColumns();
    }
	
	public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
 
    public function getGridUrl()
    {
      return $this->getUrl('*/*/grid', array('_current'=>true));
    }
 
}