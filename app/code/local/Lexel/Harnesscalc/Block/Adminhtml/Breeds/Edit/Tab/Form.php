<?php
 
class Lexel_Harnesscalc_Block_Adminhtml_Breeds_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('breeds_form', array('legend'=>Mage::helper('harnesscalc')->__('Item information')));
       
        $fieldset->addField('breed_name', 'text', array(
            'label'     => Mage::helper('harnesscalc')->__('Breed Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'breed_name',
        )); 
		
		$fieldset->addField('male_harness_size', 'text', array(
            'label'     => Mage::helper('harnesscalc')->__('Male Harness Size'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'male_harness_size',
        )); 
		
		$fieldset->addField('female_harness_size', 'text', array(
            'label'     => Mage::helper('harnesscalc')->__('Female Harness Size'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'female_harness_size',
        )); 
		
		$fieldset->addField('male_harness_puppy_size', 'text', array(
            'label'     => Mage::helper('harnesscalc')->__('Male Puppy Harness Size'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'male_harness_puppy_size',
        ));
		
		$fieldset->addField('female_harness_puppy_size', 'text', array(
            'label'     => Mage::helper('harnesscalc')->__('Female Puppy Harness Size'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'female_harness_puppy_size',
        ));  

        if ( Mage::getSingleton('adminhtml/session')->getBreedsData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBreedsData());
            Mage::getSingleton('adminhtml/session')->setBreedsData(null);
        } elseif ( Mage::registry('breeds_data') ) {
            $form->setValues(Mage::registry('breeds_data')->getData());
        }
        return parent::_prepareForm();
    }
}