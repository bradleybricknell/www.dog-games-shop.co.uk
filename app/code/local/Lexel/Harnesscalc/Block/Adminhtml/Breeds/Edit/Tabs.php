<?php
 
class Lexel_Harnesscalc_Block_Adminhtml_Breeds_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
    public function __construct()
    {
        parent::__construct();
        $this->setId('breeds_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('harnesscalc')->__('News Information'));
    }
 
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('harnesscalc')->__('Item Information'),
            'title'     => Mage::helper('harnesscalc')->__('Item Information'),
            'content'   => $this->getLayout()->createBlock('harnesscalc/adminhtml_breeds_edit_tab_form')->toHtml(),
        ));
       
        return parent::_beforeToHtml();
    }
}