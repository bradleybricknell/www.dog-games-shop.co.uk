<?php 
class Lexel_Harnesscalc_Model_Breeds extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('harnesscalc/breeds');
    }
	
	public function getBreeds($term) {
		
		$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
		
		$select = $connection->select()->from('breeds', array('*'))->where('breed_name LIKE "%' . $term . '%"');            
		$rowsArray = $connection->fetchAll($select);
		
		// To do - encode ID
		$JSON_String = '';
		foreach ($rowsArray as $row) {
			$JSON_String = $JSON_String  . '{ ' . '"id"' . ":" . '"' . $row["breed_id"] . '"' . ", " . '"label"' . ":" . '"' . $row["breed_name"]  .  '"' . ", " . '"value"' . ":" . '"' . $row["breed_id"] . '"' . ' },';
		}
		
		if ($JSON_String) {
			$JSON_String = "[ " .  $JSON_String . " ]";	
			
			$lastChar = substr($JSON_String, -1);
			$lastChar = ' ' . $lastChar;
				
			$JSON_String = substr($JSON_String, 0, -1);
			$JSON_String = substr($JSON_String, 0, -1);
			$JSON_String = substr($JSON_String, 0, -1);
			
			$JSON_String .= $lastChar;
			
			echo $JSON_String;
		}
		
		
	}
}