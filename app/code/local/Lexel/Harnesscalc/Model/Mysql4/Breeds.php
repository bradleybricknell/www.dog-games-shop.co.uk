<?php
 
class Lexel_Harnesscalc_Model_Mysql4_Breeds extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {   
        $this->_init('harnesscalc/breeds', 'breed_id');
    }
}