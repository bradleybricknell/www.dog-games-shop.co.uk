<?php
 
class Lexel_Harnesscalc_Model_Mysql4_Breeds_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('harnesscalc/breeds');
    }
}